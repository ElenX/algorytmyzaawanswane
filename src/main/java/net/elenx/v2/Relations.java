package net.elenx.v2;

import lombok.Value;

import java.util.Collection;
import java.util.NavigableSet;
import java.util.TreeSet;
import java.util.function.Predicate;
import java.util.stream.Stream;

@Value
public class Relations
{
    NavigableSet<Relation> relations = new TreeSet<>();
    Informator informator = new Informator();
    
    void add(Relation relation)
    {
        if(relations.contains(relation))
        {
            return;
        }
        
        if(relations.add(relation))
        {
            informator.inform("Dodajemy do relacji " + relation);
        }
    }
    
    void addAll(Collection<Relation> relations)
    {
        relations.forEach(this::add);
    }
    
    Stream<Relation> stream()
    {
        return relations.stream();
    }
    
    Relation first()
    {
        return relations.first();
    }
    
    void remove(Relation relation)
    {
        informator.inform("Z relacji usuwam " + relation);
        relations.remove(relation);
    }
    
    void removeIf(Predicate<Relation> predicate)
    {
        relations.removeIf(predicate);
    }
}
