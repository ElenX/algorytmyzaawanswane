package net.elenx.v2;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Value;
import lombok.experimental.NonFinal;

import java.util.LinkedList;
import java.util.List;

@Value
class Vertex
{
    @NonFinal @Getter(AccessLevel.PACKAGE)
    static int indexCounter = 1;
    
    int index;
    List<Vertex> children;
    
    Vertex(int index)
    {
        this.index = index;
        children = new LinkedList<>();
        
        indexCounter = Math.max(indexCounter, index);
    }
    
    Vertex(List<Vertex> children)
    {
        index = ++indexCounter;
        this.children = children;
    }
    
    public String toString()
    {
        return String.valueOf(index);
    }
}
