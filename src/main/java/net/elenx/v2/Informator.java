package net.elenx.v2;

import lombok.Value;

@Value
class Informator
{
    private final boolean shouldDisplay = false;
    
    void inform()
    {
        inform("");
    }
    
    void inform(Object o)
    {
        inform(o.toString());
    }
    
    void inform(String s)
    {
        if(shouldDisplay)
        {
            System.out.println(s);
        }
    }
}
