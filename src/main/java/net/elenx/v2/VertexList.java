package net.elenx.v2;

import lombok.Value;

import java.util.List;

@Value
class VertexList
{
    int distance;
    List<Vertex> vertices;
}
