package net.elenx.v2;

import lombok.Value;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Value
public class Solver
{
    Relations relations;
    Answer answer = new Answer();
    Informator informator = new Informator();
    
    public Answer solve()
    {
        while(relations.stream().distinct().count() >= 1)
        {
            Relation relation = relations.first();
    
            performStep(relation);
            informator.inform();
        }
        
        return answer;
    }
    
    private void performStep(Relation relation)
    {
        Vertex vertex = relation.getVertex1();
        VertexList nearestNodes = nearestVertexes(vertex);
    
        informator.inform("Rozwazam relacje " + relation);
        informator.inform("Najblizsze wierzcholki do wierzcholka " + vertex + " to " + nearestNodes.getVertices());
    
        int nearestDistance = nearestNodes.getDistance();
        informator.inform("Najkrotszy dystans miedzy wierzcholkami wynosi " + nearestDistance);
        
        switch(nearestDistance)
        {
            case 1:
                List<Vertex> vertices = nearestNodes.getVertices();
                
                vertices
                    .stream()
                    .map(activeVertex -> new Relation(vertex, activeVertex, nearestDistance))
                    .forEach(activeRelation -> {answer.add(activeRelation); relations.remove(activeRelation);});
                
                informator.inform("Odleglosc " + vertex + " od " + vertices + " wynosila 1, wiec dodaje je do odpowiedzi i usuwam z relacji");
                break;
            case 2:
                List<Vertex> children = nearestNodes.getVertices();
                children.add(vertex);
                Vertex newVertex = new Vertex(children);
    
                List<Relation> newRelations = children
                    .stream()
                    .map(child -> new Relation(child, newVertex, 1))
                    .collect(Collectors.toList());
                
                answer.addAll(newRelations);
                
                relations.removeIf(relation1 -> relation1.contains(vertex) && relation1.getDistance() == 2);
                informator.inform("Z relacji usuwam te ktore sa blizniakiem " + vertex);
                
                replaceVertex(children, newVertex, 1);
                
                informator.inform(children + " mialy odleglosc od siebie 2, wiec zgrupowalem je pod nowym wierzcholkiem " + newVertex);
                break;
            default:
                List<Vertex> nearestVertices = nearestNodes.getVertices();
    
                int distance;
                
                if(nearestVertices.get(0).equals(relation.getVertex2()) && nearestVertices.size() > 1)
                {
                    distance = distanceBetween(nearestVertices.get(1), relation.getVertex2());
                    informator.inform("Odleglosc miedzy wierzcholkiem " + nearestVertices.get(1) + " i wierzcholkiem " + relation.getVertex2() + " to " + distance);
                }
                else if(!nearestVertices.get(0).equals(relation.getVertex2()))
                {
                    distance = distanceBetween(nearestVertices.get(0), relation.getVertex2());
                    informator.inform("Odleglosc miedzy wierzcholkiem " + nearestVertices.get(0) + " i wierzcholkiem " + relation.getVertex2() + " to " + distance);
                }
                else
                {
                    throw new RuntimeException();
                }
    
                int x = relation.getDistance() - distance + nearestDistance;
                x = (x / 2) - 1;
    
                List<Vertex> children1 = Collections.singletonList(vertex);
                Vertex newVertex2 = new Vertex(children1);
    
                answer.add(new Relation(newVertex2, vertex, x));
    
                replaceVertex(children1, newVertex2, x);
                
                informator.inform("Tworze nowego rodzica " + newVertex2 + " w odleglosci " + x + " od jego dziecka " + vertex);
        }
    }
    
    private int distanceBetween(Vertex vertex1, Vertex vertex2)
    {
        return relations
            .stream()
            .filter(relation -> relation.contains(vertex1))
            .filter(relation -> relation.contains(vertex2))
            .findAny()
            .orElseThrow(RuntimeException::new)
            .getDistance();
    }
    
    private void replaceVertex(List<Vertex> children, Vertex newVertex, int x)
    {
        List<Relation> foldedRelationsWithVertex1 = foldRelations(children, newVertex, x, Relation::getVertex1, Relation::getVertex2);
        List<Relation> foldedRelationsWithVertex2 = foldRelations(children, newVertex, x, Relation::getVertex2, Relation::getVertex1);
        
        relations.addAll(foldedRelationsWithVertex1);
        relations.addAll(foldedRelationsWithVertex2);
    }
    
    private List<Relation> foldRelations(List<Vertex> children, Vertex newVertex, int x, Function<Relation, Vertex> firstSelector, Function<Relation, Vertex> secondSelector)
    {
        List<Relation> relationsToAdd = new LinkedList<>();
        
        List<Relation> relationsWithVertex1 = relations
            .stream()
            .filter(relation -> children.contains(firstSelector.apply(relation)))
            .collect(Collectors.toList());
    
        for(Relation relation : relationsWithVertex1)
        {
            relations.remove(relation);
        
            if(newVertex == secondSelector.apply(relation) || relation.getDistance() == 1)
            {
                continue;
            }
    
            relationsToAdd.add(new Relation(newVertex, secondSelector.apply(relation), relation.getDistance() - x));
        }
        
        return relationsToAdd;
    }
    
    private VertexList nearestVertexes(Vertex activeVertex)
    {
        Integer distance = relations
            .stream()
            .filter(relation -> relation.contains(activeVertex))
            .mapToInt(Relation::getDistance)
            .min()
            .orElseThrow(RuntimeException::new);
    
        List<Vertex> collect = relations
            .stream()
            .filter(relation -> relation.getDistance() == distance)
            .filter(relation -> relation.contains(activeVertex))
            .map(relation -> relation.getVertex1().equals(activeVertex) ? relation.getVertex2() : relation.getVertex1())
            .distinct()
            .collect(Collectors.toList());
        
        return new VertexList(distance, collect);
    }
}
