package net.elenx.v2;

import lombok.SneakyThrows;

import java.io.File;
import java.util.Scanner;

public class Main
{
    public static void main(String[] args)
    {
        Relations relations = new Main().readFrom(args[0]);
        new Informator().inform();
//        Relations relations = new Main().readFrom("/home/elentirald/IdeaProjects/AlgorytmyZaawansowane/src/main/resources/net/elenx/v2/sample4.txt");
        Answer answer = new Solver(relations).solve();
    
        System.out.print(Vertex.getIndexCounter());
        System.out.println(answer);
    }
    
    @SneakyThrows
    public Relations readFrom(String path)
    {
        File file = new File(path);
        Scanner scanner = new Scanner(file);
    
        Relations relations = new Relations();
        
        scanner.nextInt(); // drop number of vertexes as redundant
        
        while(scanner.hasNext())
        {
            Vertex firstVertex = new Vertex(scanner.nextInt());
            Vertex secondVertex = new Vertex(scanner.nextInt());
            Relation relation = new Relation(firstVertex, secondVertex, scanner.nextInt());
            
            relations.add(relation);
        }
        
        return relations;
    }
}
