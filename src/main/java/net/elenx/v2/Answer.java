package net.elenx.v2;

import lombok.Value;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

@Value
public class Answer
{
    Set<Relation> relations = new LinkedHashSet<>();
    Informator informator = new Informator();
    
    public String toString()
    {
        return relations
            .stream()
            .map(Relation::toString)
            .reduce("", (s, s2) -> String.format("%s\n%s", s, s2));
    }
    
    void addAll(Collection<Relation> relationCollection)
    {
        relationCollection.forEach(this::add);
    }
    
    void add(Relation relation)
    {
        informator.inform("Dodaje do odpowiedzi " + relation);
        relations.add(relation);
    }
}
