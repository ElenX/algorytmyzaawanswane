package net.elenx.v2;

import lombok.EqualsAndHashCode;
import lombok.Value;

@Value
@EqualsAndHashCode
public class Relation implements Comparable<Relation>
{
    Vertex vertex1;
    Vertex vertex2;
    int distance;
    
    Relation(Vertex vertex1, Vertex vertex2, int distance)
    {
        this.distance = distance;

        if(vertex1.getIndex() > vertex2.getIndex())
        {
            this.vertex2 = vertex1;
            this.vertex1 = vertex2;
        }
        else
        {
            this.vertex1 = vertex1;
            this.vertex2 = vertex2;
        }
    }
    
    boolean contains(Vertex vertex)
    {
        return vertex1.equals(vertex) || vertex2.equals(vertex);
    }
    
    public String toString()
    {
        return String.format("%s %s %d", vertex1, vertex2, distance);
    }
    
    @Override
    public int compareTo(Relation relation)
    {
        int difference = relation.distance - distance;
        
        if(difference != 0)
        {
            return difference;
        }
    
        difference = vertex1.getIndex() - relation.vertex1.getIndex();
    
        if(difference != 0)
        {
            return difference;
        }
    
        return vertex2.getIndex() - relation.vertex2.getIndex();
    }
}
