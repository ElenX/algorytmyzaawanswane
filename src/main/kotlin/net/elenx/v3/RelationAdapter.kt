package net.elenx.v3

import java.io.File
import java.util.*

object RelationAdapter
{
    fun parse(filePath: String) = parse(File(filePath))

    fun parse(file: File) = parse(Scanner(file))

    fun parse(scanner: Scanner) = scanner.useDelimiter("\r\n").asSequence().let { parse(it) }

    fun parse(lines: Sequence<String>) = lines
        .map { it.split(" ") }
        .filter { it.size >= 3 }
        .map { Triple(it[0], it[1], it[2]) }
        .map { it.mapAll { it.toInt() } }
        .map { Relation(Vertex(it.first), Vertex(it.second), it.third) }
        .toCollection(TreeSet())
}

fun <BEFORE, AFTER> Triple<BEFORE, BEFORE, BEFORE>.mapAll(function: (BEFORE) -> AFTER) = Triple(function(first), function(second), function(third))