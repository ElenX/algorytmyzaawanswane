package net.elenx.v3

import java.util.*

class Solver
{
    val PARENT_TO_CHILD_DISTANCE = 1
    val SIBLINGS_DISTANCE = 2

    val informator = Informator()
    val solverHelper = SolverHelper()

    fun solve(relations: MutableSet<Relation>): MutableSet<Relation>
    {
        val answer: MutableSet<Relation> = TreeSet()
        val vertexFactory: VertexFactory = solverHelper.vertexFactoryFor(relations)

        while (relations.count() >= 1)
        {
            val inductiveStep = performStep(relations, vertexFactory)

            relations += inductiveStep.relationsToAdd
            relations -= inductiveStep.relationsToSubtract
            answer += inductiveStep.answerExpansion
        }

        return answer
    }

    private fun performStep(relations: MutableSet<Relation>, vertexFactory: VertexFactory): InductiveStep
    {
        val relation = relations.first()
        val vertex = relation.first
        val vertexGroup = solverHelper.nearestVertexes(vertex, relations)

        informator.inform("Rozwazam relacje " + relation)
        informator.inform("Najblizsze wierzcholki do wierzcholka $vertex to ${vertexGroup.vertexes}")
        informator.inform("Najkrotszy dystans miedzy wierzcholkami wynosi ${vertexGroup.distance}")

        return inductiveStep(vertexGroup, vertex, relations, vertexFactory, relation)
    }

    private fun inductiveStep(vertexGroup: VertexGroup, vertex: Vertex, relations: Set<Relation>, vertexFactory: VertexFactory, relation: Relation) = when (vertexGroup.distance)
    {
        PARENT_TO_CHILD_DISTANCE -> extractParentWithChildren(vertex, vertexGroup)
        SIBLINGS_DISTANCE -> extractSiblings(vertexGroup.vertexes + vertex, vertex, vertexFactory.vertex(vertexGroup.vertexes), relations)
        else -> cutDistance(vertexGroup, relation, relations, vertex, vertexFactory)
    }

    private fun extractParentWithChildren(vertex: Vertex, vertexGroup: VertexGroup) =
        vertexGroup
            .vertexes
            .map { Relation(vertex, it, vertexGroup.distance) }
            .map { InductiveStep(answerExpansion = it, relationToSubtract = it) }
            .reduce(InductiveStep::plus)
            .also { informator.inform("Odleglosc $vertex od ${vertexGroup.vertexes} wynosila 1, wiec dodaje je do odpowiedzi i usuwam z relacji") }

    private fun extractSiblings(children: List<Vertex>, child: Vertex, parent: Vertex, relations: Set<Relation>): InductiveStep
    {
        informator.inform("Z relacji usuwam te ktore sa blizniakiem $child")
        informator.inform("$children mialy odleglosc od siebie 2, wiec zgrupowalem je pod nowym wierzcholkiem $parent")

        return solverHelper.replaceVertex(children, parent, PARENT_TO_CHILD_DISTANCE, relations) + solverHelper.linkParentWithChildren(children, parent, child, relations)
    }

    private fun cutDistance(vertexGroup: VertexGroup, relation: Relation, relations: Set<Relation>, vertex: Vertex, vertexFactory: VertexFactory): InductiveStep
    {
        val distanceToCut = distanceToCut(vertexGroup, relation, relations)
        val children = listOf(vertex)
        val newVertex = vertexFactory.vertex(children)

        return solverHelper
            .replaceVertex(children, newVertex, distanceToCut, relations) + InductiveStep(answerExpansion = Relation(newVertex, vertex, distanceToCut))
            .also { informator.inform("Tworze nowego rodzica $newVertex w odleglosci $distanceToCut od jego dziecka $vertex") }
    }

    @Suppress("ReplaceSingleLineLet")
    private fun distanceToCut(vertexGroup: VertexGroup, relation: Relation, relations: Set<Relation>) =
        solverHelper
            .distanceTo(vertexGroup.vertexes, relation.second, relations)
            .let { relation.distance - it + vertexGroup.distance}
            .let { it / 2 }
            .let { it - 1 }
}