package net.elenx.v3

import java.util.*

class SolverHelper
{
    val informator = Informator()

    fun parentFor(nearestVertices: List<Vertex>, newVertex: Vertex) =
        nearestVertices
            .map { Relation(it, newVertex, 1) }
            .toSet()

    fun siblingsOf(vertex: Vertex, relations: Set<Relation>) =
        relations
            .filter { it.distance == 2 }
            .filter { it.contains(vertex) }
            .toSet()

    fun vertexFactoryFor(relations: Set<Relation>) =
        relations
            .map { if (it.first > it.second) it.first else it.second }
            .map { it.index }
            .max()
            .let { it ?: throw RuntimeException() }
            .let { VertexFactory(it) }

    fun distanceTo(nearestVertices: List<Vertex>, secondVertex: Vertex, relations: Set<Relation>) = when
    {
        nearestVertices[0] != secondVertex -> distanceBetween(nearestVertices[0], secondVertex, relations)
        else -> distanceBetween(nearestVertices[1], secondVertex, relations)
    }

    private fun distanceBetween(firstVertex: Vertex, secondVertex: Vertex, relations: Set<Relation>) =
        relations
            .first { it.contains(firstVertex) && it.contains(secondVertex) }
            .distance
            .also { informator.inform("Odleglosc miedzy wierzcholkiem $firstVertex i wierzcholkiem $secondVertex to $it") }

    fun nearestVertexes(vertex: Vertex, relations: Set<Relation>) =
        relationsWith(vertex, relations)
            .filter { it.distance == minimalDistanceTo(vertex, relations) }
            .map { if (it.first == vertex) it.second else it.first }
            .distinct()
            .let { VertexGroup(minimalDistanceTo(vertex, relations), LinkedList(it)) }

    private fun minimalDistanceTo(vertex: Vertex, relations: Set<Relation>) =
        relationsWith(vertex, relations)
            .map { it.distance }
            .min() ?: throw RuntimeException("There are no vertexes")

    private fun relationsWith(vertex: Vertex, relations: Set<Relation>) = relations.filter { it.contains(vertex) }

    fun replaceVertex(children: List<Vertex>, newVertex: Vertex, distanceToCut: Int, relations: Set<Relation>): InductiveStep
    {
        val relationsToSubtract = relations.filter { children.contains(it.first) || children.contains(it.second) }.toSet()
        val relationsToAdd = relationsWithCutDistance(children, newVertex, distanceToCut, relations)

        return InductiveStep(relationsToSubtract = relationsToSubtract, relationsToAdd = relationsToAdd)
    }

    private fun relationsWithCutDistance(children: List<Vertex>, newVertex: Vertex, distanceToCut: Int, relations: Set<Relation>) =
        relationsWithCutDistance(children, newVertex, distanceToCut, relations, Relation::first, Relation::second) +
            relationsWithCutDistance(children, newVertex, distanceToCut, relations, Relation::second, Relation::first)

    private fun relationsWithCutDistance(children: List<Vertex>, newVertex: Vertex, distanceToCut: Int, relations: Set<Relation>, firstSelector: (Relation) -> Vertex, secondSelector: (Relation) -> Vertex) =
        relations
            .filterNot { it.distance == 1 }
            .filterNot { newVertex == secondSelector(it) }
            .filter { children.contains(firstSelector(it)) }
            .map { Relation(newVertex, secondSelector(it), it.distance - distanceToCut) }
            .toSet()

    fun linkParentWithChildren(children: List<Vertex>, parent: Vertex, child: Vertex, relations: Set<Relation>)
        = InductiveStep(answerExpansion = parentFor(children, parent), relationsToSubtract = siblingsOf(child, relations))
}