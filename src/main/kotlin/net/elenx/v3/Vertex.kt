package net.elenx.v3

data class Vertex(val index: Int, val children: List<Vertex>) : Comparable<Vertex>
{
    constructor(index: Int) : this(index, listOf())

    override fun equals(other: Any?) = other is Vertex && index == other.index

    override fun compareTo(other: Vertex) = index - other.index

    override fun toString() = index.toString()

    override fun hashCode() = 31 * index + children.hashCode()
}

data class VertexGroup(val distance: Int, val vertexes: List<Vertex>)

class VertexFactory(var activeIndex: Int)
{
    fun vertex(children: List<Vertex>) = Vertex(++activeIndex, children)
}
