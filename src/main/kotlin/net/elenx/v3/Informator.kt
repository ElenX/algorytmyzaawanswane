package net.elenx.v3

class Informator
{
    val shouldDisplay = true

    fun inform(message: String) = if(shouldDisplay) println(message) else { }
}