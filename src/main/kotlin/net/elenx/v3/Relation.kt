package net.elenx.v3

class Relation(first: Vertex, second: Vertex, val distance: Int) : Comparable<Relation>
{
    val first: Vertex
    val second: Vertex

    init
    {
        if(first.index < second.index)
        {
            this.first = first
            this.second = second
        }
        else if(first.index > second.index)
        {
            this.first = second
            this.second = first
        }
        else
        {
            throw RuntimeException("Cannot create relation between two same vertexes")
        }
    }

    fun contains(vertex: Vertex) = first == vertex || second == vertex

    override fun compareTo(other: Relation) = when
    {
        other.distance - distance != 0 -> other.distance - distance
        other.first.index - first.index != 0 -> other.first.index - first.index
        else -> other.second.index - second.index
    }

    override fun equals(other: Any?) = other is Relation && first == other.first && second == other.second && distance == other.distance

    override fun toString() = String.format("%s %s %d", first, second, distance)

    override fun hashCode(): Int
    {
        var result = distance
        result = 31 * result + first.hashCode()
        result = 31 * result + second.hashCode()

        return result
    }
}
