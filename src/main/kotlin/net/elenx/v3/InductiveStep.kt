package net.elenx.v3

private fun set(relation: Relation?) = if(relation == null) setOf() else setOf(relation)

data class InductiveStep(val answerExpansion: Set<Relation> = setOf(), val relationsToSubtract: Set<Relation> = setOf(), val relationsToAdd: Set<Relation> = setOf())
{
    constructor(answerExpansion: Relation? = null, relationToSubtract: Relation? = null, relationToAdd: Relation? = null) : this(set(answerExpansion), set(relationToSubtract), set(relationToAdd))

    operator fun plus(other: InductiveStep): InductiveStep
    {
        val newRelationsToAdd = relationsToAdd + other.relationsToAdd
        val newRelationsToSubtract = relationsToSubtract + other.relationsToSubtract
        val newAnswerExpansion = answerExpansion + other.answerExpansion

        return InductiveStep(newAnswerExpansion, newRelationsToSubtract, newRelationsToAdd)
    }
}