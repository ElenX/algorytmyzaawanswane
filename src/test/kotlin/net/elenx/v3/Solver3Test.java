package net.elenx.v3;

import java.util.Collection;
import java.util.stream.Stream;

class Solver3Test
{
    static String solve(String path)
    {
        return Stream
            .of(RelationAdapter.INSTANCE.parse(path))
            .map(relations -> new Solver().solve(relations))
            .flatMap(Collection::stream)
            .map(Relation::toString)
            .sorted()
            .reduce((s, s2) -> s + "\r\n" + s2)
            .orElseThrow(RuntimeException::new);
    }
}
