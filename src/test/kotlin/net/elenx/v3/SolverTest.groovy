package net.elenx.v3

import spock.lang.Specification

class SolverTest extends Specification
{
    void "should return same results as java"()
    {
        when:
        def solutions = Arrays
            .stream(new File("src/test/resources/net/elenx/v3"))
            .map { it.listFiles() }
            .map { Arrays.asList(it) }
            .flatMap { it.stream() }
            .limit(4)
            .map { it.absolutePath }
            .peek { println it}
            .map { new Tuple2(Solver2Test.solve(it), Solver3Test.solve(it)) }
            .filter { it.first != it.second }
            .peek { println it }

        then:
            solutions.count() == 0
    }
}
