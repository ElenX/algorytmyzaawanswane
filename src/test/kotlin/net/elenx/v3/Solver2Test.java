package net.elenx.v3;

import net.elenx.v2.Answer;
import net.elenx.v2.Main;
import net.elenx.v2.Relation;
import net.elenx.v2.Solver;

import java.util.Collection;
import java.util.stream.Stream;

class Solver2Test
{
    static String solve(String path)
    {
        return Stream
            .of(new Main().readFrom(path))
            .map(Solver::new)
            .map(Solver::solve)
            .map(Answer::getRelations)
            .flatMap(Collection::stream)
            .map(Relation::toString)
            .sorted()
            .reduce((s, s2) -> s + "\r\n" + s2)
            .orElseThrow(RuntimeException::new);
    }
}
